import Vue from 'vue'
import Vuex from 'vuex'
import { mutations } from './mutations'
import * as actions from './actions'


import VueA11yDialog from 'vue-a11y-dialog'

Vue.use(VueA11yDialog)

Vue.use(Vuex)

const state = {
  user: {},
  events: []
}

export default new Vuex.Store({
 state,
 mutations,
 actions
})
