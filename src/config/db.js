
import firebase from 'firebase'

var config = {
  apiKey: "AIzaSyCjkLb1qE1N-hYj2O-QUtU0M2M-HFNOf60",
  authDomain: "simple-trello-9a4a8.firebaseapp.com",
  databaseURL: "https://simple-trello-9a4a8.firebaseio.com",
  projectId: "simple-trello-9a4a8",
  storageBucket: "simple-trello-9a4a8.appspot.com",
  messagingSenderId: "668721121033"
};

export const app = firebase.initializeApp(config);
export const eventsRef = app.database().ref().child('todos');
export const allUsersRef = app.database().ref().child('allUsers');
export const db = app.database();
